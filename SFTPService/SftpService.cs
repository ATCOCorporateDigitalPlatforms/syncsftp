﻿using System;
using System.Collections.Generic;
using System.IO;
using Renci.SshNet;
using Renci.SshNet.Sftp;

namespace SFTPService
{
    public interface ISftpService
    {
        IEnumerable<SftpFile> ListAllFiles(string remoteDirectory = ".");
        void UploadFile(string localFilePath, string remoteFilePath);
        void DownloadFile(string remoteFilePath, string localFilePath);
        void SynchronizeOutbox(string remotePath, string localPath, bool purge);
        void SynchronizeInbox(string remotePath, string localPath, bool purge);
        void DeleteFile(string remoteFilePath);
    }

    public class SftpService : ISftpService
    {
        private readonly SftpConfig _config;

        public SftpService(SftpConfig sftpConfig)
        {
            _config = sftpConfig;
        }

        public IEnumerable<SftpFile> ListAllFiles(string remoteDirectory = ".")
        {
            using var client = new SftpClient(_config.Host, _config.Port == 0 ? 22 : _config.Port, _config.UserName, _config.Password);
            try
            {
                client.Connect();
                return client.ListDirectory(remoteDirectory);
            }
            catch (Exception exception)
            {
                Console.WriteLine($"Failed in listing files under [{remoteDirectory}]: {exception}");
                return null;
            }
            finally
            {
                client.Disconnect();
            }
        }

        public void UploadFile(string localFilePath, string remoteFilePath)
        {
            using var client = new SftpClient(_config.Host, _config.Port == 0 ? 22 : _config.Port, _config.UserName, _config.Password);
            try
            {
                client.Connect();
                using var s = File.OpenRead(localFilePath);
                client.UploadFile(s, remoteFilePath);
                Console.WriteLine($"Finished uploading file [{localFilePath}] to [{remoteFilePath}]");
            }
            catch (Exception exception)
            {
                Console.WriteLine($"Failed in uploading file [{localFilePath}] to [{remoteFilePath}]: {exception}");
            }
            finally
            {
                client.Disconnect();
            }
        }

        public void DownloadFile(string remoteFilePath, string localFilePath)
        {
            using var client = new SftpClient(_config.Host, _config.Port == 0 ? 22 : _config.Port, _config.UserName, _config.Password);
            try
            {
                client.Connect();
                using var s = File.Create(localFilePath);
                client.DownloadFile(remoteFilePath, s);
                Console.WriteLine($"Finished downloading file [{localFilePath}] from [{remoteFilePath}]");
            }
            catch (Exception exception)
            {
                Console.WriteLine($"Failed in downloading file [{localFilePath}] from [{remoteFilePath}]: {exception}");
            }
            finally
            {
                client.Disconnect();
            }
        }

        public void DeleteFile(string remoteFilePath)
        {
            using var client = new SftpClient(_config.Host, _config.Port == 0 ? 22 : _config.Port, _config.UserName, _config.Password);
            try
            {
                client.Connect();
                client.DeleteFile(remoteFilePath);
                Console.WriteLine($"File [{remoteFilePath}] deleted.");
            }
            catch (Exception exception)
            {
                Console.WriteLine($"Failed in deleting file [{remoteFilePath}]: {exception}");
}
            finally
                        {
                client.Disconnect();
            }
        }

        public void SynchronizeOutbox(string remotePath, string localPath, bool purge)
        {

            int newCountTotal = 0;
            int newCountSuccess = 0;
            int newCountFail = 0;

            int skipCountTotal = 0;

            int updateCountTotal = 0;
            int updateCountSuccess = 0;
            int updateCountFail = 0;

            int deleteCountTotal = 0;
            int deleteCountSuccess = 0;
            int deleteCountFail = 0;

            int totalCount = 0;
            int totalError = 0;

            using var client = new SftpClient(_config.Host, _config.Port == 0 ? 22 : _config.Port, _config.UserName, _config.Password);
            
            client.Connect();

            DirectoryInfo di = new(localPath);

            FileInfo[] files = di.GetFiles();

            foreach (FileInfo file in files)
            {
                totalCount += 1;

                bool success = false;

                string fn = file.Name;
                string localFile = file.FullName;

                DateTime localWriteStamp = file.LastWriteTimeUtc;

                string remoteFile = Path.Combine(remotePath, fn);

                if (client.Exists(remoteFile))
                {
                    SftpFileAttributes rfileatt = client.GetAttributes(remoteFile);
                    DateTime remoteWriteStamp = rfileatt.LastWriteTimeUtc;
                    
                    if (remoteWriteStamp < localWriteStamp)
                    {
                        // Local file is newer
                        updateCountTotal += 1;

                        try
                        {
                            updateCountSuccess += 1;

                            client.Delete(remoteFile);

                            using var s = File.OpenRead(localFile);
                            client.UploadFile(s, remoteFile);

                            Console.WriteLine($"Local [{fn}] is newer, replacing remote");

                            success = true;
                        }
                        catch (Exception e)
                        {
                            updateCountFail += 1;
                            totalError += 1;

                            Console.WriteLine($"Local [{fn}] is newer, error during upload: {e}");
                        }
                    }
                    else
                    {
                        skipCountTotal += 1;

                        Console.WriteLine($"Remote [{fn}] is newer, skipping file");
                    }
                }
                else
                {
                    newCountTotal += 1;
                    try
                    {
                        newCountSuccess += 1;
                        using var s = File.OpenRead(localFile);
                        client.UploadFile(s, remoteFile);

                        Console.WriteLine($"Local [{fn}] is new, uploading to remote");

                        success = true;
                    }
                    catch (Exception e)
                    {
                        newCountFail += 1;
                        totalError += 1;
                        Console.WriteLine($"Local [{fn}] is new, error during upload: {e}");
                    }
                }

                if (purge && success)
                {
                    deleteCountTotal += 1;

                    try
                    {

                        deleteCountSuccess += 1;
                        Console.WriteLine($"Deleted local [{fn}]");
                        file.Delete();
                    }
                    catch (Exception e)
                    {
                        deleteCountFail += 1;
                        totalError += 1;
                        Console.WriteLine($"Failed to delete local [{fn}]: {e}");
                    }
                }
            }

            client.Disconnect();

            Console.WriteLine($"New files copied: {newCountSuccess}");
            Console.WriteLine($"New file errors: {newCountFail}");
            Console.WriteLine($"TOTAL NEW FILES: {newCountTotal}");

            Console.WriteLine($"Updated files replaced: {updateCountSuccess}");
            Console.WriteLine($"Updated file errors: {updateCountFail}");
            Console.WriteLine($"TOTAL UPDATED FILES: {updateCountTotal}");

            Console.WriteLine($"TOTAL SKIPPED FILES: {skipCountTotal}");

            Console.WriteLine($"TOTAL DELETED REMOTE FILES: {deleteCountSuccess}");

            Console.WriteLine($"TOTAL FILES PROCESSED: {totalCount}");
        }

        public void SynchronizeInbox(string remotePath, string localPath, bool purge)
        {
            int newCountTotal = 0;
            int newCountSuccess = 0;
            int newCountFail = 0;

            int skipCountTotal = 0;

            int updateCountTotal = 0;
            int updateCountSuccess = 0;
            int updateCountFail = 0;

            int deleteCountTotal = 0;
            int deleteCountSuccess = 0;
            int deleteCountFail = 0;

            int totalCount = 0;
            int totalError = 0;

            using var client = new SftpClient(_config.Host, _config.Port == 0 ? 22 : _config.Port, _config.UserName, _config.Password);

            if (!Directory.Exists(localPath))
            {
                try
                {
                    Directory.CreateDirectory(localPath);
                    Console.WriteLine($"Local folder [{localPath}] created.");
         
                }
                catch (Exception exception)
                {
                    Console.WriteLine($"Failed to create local folder [{localPath}]: {exception}");
                }
            }

            client.Connect();
            var files = client.ListDirectory(remotePath);

            foreach (var file in files)
            {
                if (file.IsRegularFile)
                {
                    totalCount += 1;

                    DateTime fileWriteStamp = file.LastWriteTimeUtc;

                    string localFile = Path.Combine(localPath, file.Name);

                    bool success = false;

                    if (File.Exists(localFile))
                    {

                        DateTime localWriteStamp = File.GetLastWriteTimeUtc(localFile);
                        if (localWriteStamp < fileWriteStamp)
                        {
                            updateCountTotal += 1;

                            try
                            {
                                updateCountSuccess += 1;

                                File.Delete(localFile);

                                using var s = File.Create(localFile);
                                client.DownloadFile(file.FullName, s);

                                Console.WriteLine($"[{file.Name}] remote copy is newer, local file replaced");

                                success = true;
                            }
                            catch (Exception exception)
                            {
                                updateCountFail += 1;
                                totalError += 1;

                                Console.WriteLine($"[{file.Name}] failed to update with newer remote copy: {exception}");
                            }
                        }
                        else
                        {
                            skipCountTotal += 1;

                            Console.WriteLine($"[{file.Name}] local copy is newer, skipping file");

                            success = true;
                        }
                    }
                    else
                    {
                        newCountTotal += 1;

                        try
                        {
                            newCountSuccess += 1;

                            using var s = File.Create(localFile);
                            client.DownloadFile(file.FullName, s);
                            Console.WriteLine($"[{file.Name}] is new, syncing to local");

                            success = true;
                        }
                        catch (Exception exception)
                        {
                            newCountFail += 1;
                            totalError += 1;

                            Console.WriteLine($"[{file.Name}] is new, failed to create local: {exception}");
                        }
                    }

                    if (purge && success)
                    {
                        deleteCountTotal += 1;

                        try
                        {

                            deleteCountSuccess += 1;
                            Console.WriteLine($"Deleted remote [{file.Name}]");
                            file.Delete();
                        }
                        catch (Exception e)
                        {
                            deleteCountFail += 1;
                            Console.WriteLine($"Failed to delete remote [{file.Name}]: {e}");
                        }
                    }
                }
            }

            client.Disconnect();

            Console.WriteLine($"New files copied: {newCountSuccess}");
            Console.WriteLine($"New file errors: {newCountFail}");
            Console.WriteLine($"TOTAL NEW FILES: {newCountTotal}");

            Console.WriteLine($"Updated files replaced: {updateCountSuccess}");
            Console.WriteLine($"Updated file errors: {updateCountFail}");
            Console.WriteLine($"TOTAL UPDATED FILES: {updateCountTotal}");

            Console.WriteLine($"TOTAL SKIPPED FILES: {skipCountTotal}");

            Console.WriteLine($"TOTAL DELETED REMOTE FILES: {deleteCountSuccess}");

            Console.WriteLine($"TOTAL FILES PROCESSED: {totalCount}");
        }
    }
}
