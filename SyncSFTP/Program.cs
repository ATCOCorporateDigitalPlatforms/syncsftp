﻿using System;
using System.Configuration;
using SFTPService;

namespace SyncSFTP
{
    internal class Program
    {
        private static void Main(string[] args)
        {

            string hostName = null;
            int portNum = 22;
            string user = null;
            string pass = null;
            string remotePath = null;
            string localPath = null;

            if (args.Length != 3)
            {
                Console.WriteLine("Invalid number of arguments passed to SyncSFTP");
                Console.WriteLine("Expected syntax is \'SyncSFTP.exe <mode> <remotePath> <localPath>\'");
                Environment.Exit(1);
            }
            else
            {
                try
                {
                    hostName = ConfigurationManager.AppSettings["HostName"];
                    portNum = Int32.Parse(ConfigurationManager.AppSettings["PortNumber"]);
                    user = ConfigurationManager.AppSettings["UserName"];
                    pass = ConfigurationManager.AppSettings["Password"];
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Error reading config file: {e}");
                    Environment.Exit(1);
                }

                try
                {
                    remotePath = args[1];
                    localPath = args[2];
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Error setting folder paths: {e}");
                    Environment.Exit(1);
                }

                SftpService sftpService = null;

                try
                {
                    var config = new SftpConfig
                    {
                        Host = hostName,
                        Port = portNum,
                        UserName = user,
                        Password = pass
                    };

                    sftpService = new SftpService(config);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Error initializing SFTP service: {e}");
                    Environment.Exit(1);
                }

                try
                {
                    switch (args[0])
                    {
                        case "inbox-copy":
                            // Download new/updated files
                            Console.WriteLine($"Synchronizing remote [{remotePath}] to local [{localPath}]");
                            sftpService.SynchronizeInbox(remotePath, localPath, false);
                            break;
                        case "inbox-move":
                            // Download new/updated files and delete remote copies
                            Console.WriteLine($"Synchronizing remote [{remotePath}] to local [{localPath}]");
                            sftpService.SynchronizeInbox(remotePath, localPath, true);
                            break;
                        case "outbox-copy":
                            // Upload new files
                            Console.WriteLine($"Synchronizing local [{localPath}] to remote [{remotePath}]");
                            sftpService.SynchronizeOutbox(remotePath, localPath, false);
                            break;
                        case "outbox-move":
                            // Upload new files
                            Console.WriteLine($"Synchronizing local [{localPath}] to remote [{remotePath}]");
                            sftpService.SynchronizeOutbox(remotePath, localPath, true);
                            break;
                        default:
                            Console.WriteLine($"Invalid execution mode paramter [{args[0]}] provided");
                            Console.WriteLine("Valid modes are inbox-copy, inbox-move, outbox-copy, outbox-move");
                            Environment.Exit(1);
                            break;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine($"{e}");
                    Environment.Exit(1);
                }
            }
        }
    }
}
